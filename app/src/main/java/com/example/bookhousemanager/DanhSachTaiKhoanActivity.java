package com.example.bookhousemanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.bookhousemanager.R;
import com.example.bookhousemanager.adapter.TaiKhoanAdapter;
import com.example.bookhousemanager.dao.TaiKhoanDao;
import com.example.bookhousemanager.model.TaiKhoan;

import java.util.ArrayList;
import java.util.List;

public class DanhSachTaiKhoanActivity extends AppCompatActivity {
    ListView lvDanhSachTaiKhoan;
    TaiKhoanAdapter adapter;
    List<TaiKhoan> list;
    TaiKhoanDao taiKhoanDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danh_sach_tai_khoan);
        setTitle("Danh sách tài khoản");

        initComponent();
        showDanhSachTaiKhoan();
    }

    @Override
    public void onResume() {
        super.onResume();
        list.clear();
        list=taiKhoanDao.getAll();
        adapter.changeDataset(taiKhoanDao.getAll());
    }

    public void initComponent(){
        lvDanhSachTaiKhoan=findViewById(R.id.lvDanhSachTaiKhoan);
    }

    public void showDanhSachTaiKhoan(){
        list=new ArrayList<>();
        taiKhoanDao=new TaiKhoanDao(DanhSachTaiKhoanActivity.this);
        list= taiKhoanDao.getAll();
        adapter=new TaiKhoanAdapter(list,DanhSachTaiKhoanActivity.this);
        lvDanhSachTaiKhoan.setAdapter(adapter);
    }

    public void xoaTaiKhoan(TaiKhoan taiKhoan){
        taiKhoanDao.delete(taiKhoan);
        capNhatLV();
    }

    public void capNhatLV(){
        adapter.notifyDataSetChanged();
    }
}
