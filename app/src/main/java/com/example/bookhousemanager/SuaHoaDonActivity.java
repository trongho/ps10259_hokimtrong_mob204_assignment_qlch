package com.example.bookhousemanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.bookhousemanager.dao.HoaDonDao;
import com.example.bookhousemanager.model.HoaDon;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SuaHoaDonActivity extends AppCompatActivity {
    EditText edtMaHD;
    static EditText edtNgayMua;
    Button btnSua,btnHuy,btnDatePicker;
    HoaDon hoaDon;
    HoaDonDao hoaDonDao;
    static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_hoa_don);
        setTitle("Sửa hóa đơn");

        initComponent();
        sua();
        huy();
    }

    public void initComponent(){
        edtMaHD=findViewById(R.id.edtMaHD);
        edtNgayMua=findViewById(R.id.edtNgayMua);
        btnSua=findViewById(R.id.btnSua);
        btnHuy=findViewById(R.id.btnHuy);
        btnDatePicker=findViewById(R.id.btnDatePicker);
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Calendar cal=new GregorianCalendar(year,month,day);
            setDate(cal);

        }

        public void setDate(Calendar calendar){
            edtNgayMua.setText(sdf.format(calendar.getTime()));
        }
    }

    public void showDatePickerDialog() {
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
    }

    public void sua(){
        //lấy dữ liệu từ bundle
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("suahoadon");
        if(bundle!=null){
            String maHD=bundle.getString("mahd","");
            String ngayMua=bundle.getString("ngaymua","");
            edtMaHD.setText(maHD);
            edtNgayMua.setText(ngayMua);

            if(!maHD.isEmpty())
                edtMaHD.setEnabled(false);
        }

        //datepicker
        showDatePickerDialog();

        //sửa
        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hoaDon=new HoaDon();
                hoaDonDao=new HoaDonDao(SuaHoaDonActivity.this);
                hoaDon.setMaHD(edtMaHD.getText().toString());
                try {
                    hoaDon.setNgayMua(sdf.parse(edtNgayMua.getText().toString()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                hoaDonDao.update(hoaDon);
                finish();
            }
        });

    }

    public void huy(){
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
