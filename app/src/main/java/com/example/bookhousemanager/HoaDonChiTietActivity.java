package com.example.bookhousemanager;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bookhousemanager.adapter.HoaDonChiTietAdapter;
import com.example.bookhousemanager.dao.HoaDonChiTietDao;
import com.example.bookhousemanager.dao.HoaDonDao;
import com.example.bookhousemanager.dao.SachDao;
import com.example.bookhousemanager.model.HoaDon;
import com.example.bookhousemanager.model.HoaDonChiTiet;
import com.example.bookhousemanager.model.Sach;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class HoaDonChiTietActivity extends AppCompatActivity {
    EditText edtMaHD,edtMaHDCT,edtSoLuongMua;
    TextView tvMaSach;
    Spinner spMaSach;
    Button btnThem,btnThanhToan;
    RecyclerView recyclerView;
    HoaDonChiTiet hoaDonChiTiet;
    HoaDonChiTietDao hoaDonChiTietDao;
    SachDao sachDao;
    HoaDonDao hoaDonDao;
    List<HoaDonChiTiet> list;
    HoaDonChiTietAdapter adapter;
    LinearLayoutManager mLayoutManager;

    private DatabaseReference mDatabaseSach;
    Sach sach;
    HoaDon hoaDon;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoa_don_chi_tiet);
        setTitle("Hóa đơn chi tiết");

        initComponent();
        showDanhSachHDCT();
        themHDCT();
    }

    public void initComponent(){
        edtMaHD=findViewById(R.id.edtMaHD);
        edtMaHDCT=findViewById(R.id.edtMaHDCT);
        tvMaSach=findViewById(R.id.tvMaSach);
        spMaSach=findViewById(R.id.spMaSach);
        edtSoLuongMua=findViewById(R.id.edtSoLuongMua);
        btnThem=findViewById(R.id.btnThem);
        btnThanhToan=findViewById(R.id.btnThanhToan);
        recyclerView=findViewById(R.id.recycle_view);
    }

    public void layDuLieuTuBundle(){
        //lấy dữ liệu mã hóa đơn từ bundle
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("hoadonchitiet");
        if (bundle != null) {
            String maHD = bundle.getString("mahd", "");
            edtMaHD.setText(maHD);
            edtMaHD.setEnabled(false);
        }
    }

    public void showDanhSachHDCT(){
        hoaDonChiTiet=new HoaDonChiTiet();
        hoaDonChiTietDao=new HoaDonChiTietDao(HoaDonChiTietActivity.this);
        list=new ArrayList<HoaDonChiTiet>();
        list=hoaDonChiTietDao.getAll();
        mLayoutManager=new LinearLayoutManager(HoaDonChiTietActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter=new HoaDonChiTietAdapter(HoaDonChiTietActivity.this,list);
        recyclerView.setAdapter(adapter);
    }

    public void showSpinner(){
        mDatabase = FirebaseDatabase.getInstance().getReference("Sach");
        final List<String> list=new ArrayList<>();
        ValueEventListener postListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data:dataSnapshot.getChildren()){
                    String ma=data.child("maSach").getValue(String.class);
                    String ten=data.child("tenSach").getValue(String.class);
                    String item=ma+"-"+ten;
                    list.add(item);

                    ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(
                            HoaDonChiTietActivity.this,android.R.layout.simple_spinner_item,list);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spMaSach.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    public void selectItemSpinner(){
        spMaSach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvMaSach.setText(spMaSach.getSelectedItem().toString().
                        substring(0,spMaSach.getSelectedItem().toString().indexOf("-")));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void themHDCT(){
        showSpinner();
        selectItemSpinner();
        layDuLieuTuBundle();

        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hoaDonChiTiet=new HoaDonChiTiet();
                hoaDonChiTietDao=new HoaDonChiTietDao(HoaDonChiTietActivity.this);
                sachDao=new SachDao(HoaDonChiTietActivity.this);
                hoaDonDao=new HoaDonDao(HoaDonChiTietActivity.this);

                hoaDonChiTiet.setSach(sachDao.getSachByID(edtMaHD.getText().toString()));
                hoaDonChiTiet.setHoaDon(hoaDonDao.getHoaDonByID(edtMaHD.getText().toString()));
                hoaDonChiTiet.setMaHDCT(Integer.parseInt(edtMaHDCT.getText().toString()));
                hoaDonChiTiet.setSoLuongMua(Integer.parseInt(edtSoLuongMua.getText().toString()));
                hoaDonChiTietDao.insert(hoaDonChiTiet);
            }
        });

    }

    public void xoaHDCT(HoaDonChiTiet hoaDonChiTiet){
        hoaDonChiTietDao.delete(hoaDonChiTiet);
        capNhatLV();
    }

    public void suaHDCT(HoaDonChiTiet hoaDonChiTiet){
        hoaDonChiTietDao.update(hoaDonChiTiet);
        capNhatLV();
    }

    public void capNhatLV(){
        adapter.notifyItemInserted(list.size());
        adapter.notifyDataSetChanged();
    }


}
