package com.example.bookhousemanager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bookhousemanager.dao.TaiKhoanDao;
import com.example.bookhousemanager.model.TaiKhoan;

public class SuaTaiKhoanActivity extends AppCompatActivity {
    EditText edtUsername,edtPassword,edtRePassword,edtFullName,edtPhone;
    Button btnSua,btnHuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_tai_khoan);
        setTitle("Sửa tài khoản");

        initComponent();
        layDuLieu();
        sua();
        huy();
    }

    public void initComponent(){
        edtUsername=findViewById(R.id.edtUsername);
        edtPassword=findViewById(R.id.edtPassword);
        edtRePassword=findViewById(R.id.edtRePassword);
        edtFullName=findViewById(R.id.edtFullName);
        edtPhone=findViewById(R.id.edtPhone);
        btnSua=findViewById(R.id.btnSua);
        btnHuy=findViewById(R.id.btnHuy);
    }

    public void layDuLieu(){
        Intent intent=getIntent();
        String username=intent.getStringExtra("username");
        String password=intent.getStringExtra("password");
        String fullname=intent.getStringExtra("fullname");
        String phone=intent.getStringExtra("phone");
        edtUsername.setText(username);
        edtUsername.setEnabled(false);
        edtPassword.setText(password);
        edtFullName.setText(fullname);
        edtPhone.setText(phone);
    }

    public void sua(){
        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaiKhoan taiKhoan=new TaiKhoan();
                taiKhoan.setUsername(edtUsername.getText().toString());
                taiKhoan.setPassword(edtPassword.getText().toString());
                taiKhoan.setFullName(edtFullName.getText().toString());
                taiKhoan.setPhone(edtPhone.getText().toString());
                TaiKhoanDao taiKhoanDao=new TaiKhoanDao(SuaTaiKhoanActivity.this);
                taiKhoanDao.update(taiKhoan);
                finish();
            }
        });
    }

    public void huy(){
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
