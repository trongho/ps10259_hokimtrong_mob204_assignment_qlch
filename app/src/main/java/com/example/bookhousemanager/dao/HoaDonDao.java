package com.example.bookhousemanager.dao;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.example.bookhousemanager.fragment.HoaDonFragment;
import com.example.bookhousemanager.fragment.HoaDonFragment;
import com.example.bookhousemanager.model.HoaDon;
import com.example.bookhousemanager.model.HoaDon;
import com.example.bookhousemanager.model.Sach;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class HoaDonDao {
    private DatabaseReference mDatabase;
    private Context context;
    String keyID;
    HoaDonFragment fragment;
    HoaDon hoaDon;

    public HoaDonDao(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("HoaDon");
        this.context = context;
    }

    public HoaDonDao(Context context, HoaDonFragment fragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("HoaDon");
        this.context = context;
        this.fragment= fragment;
    }

    //lấy tất cả hóa đơn
    public List<HoaDon> getAll() {
        final List<HoaDon> list = new ArrayList<HoaDon>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    HoaDon item = data.getValue(HoaDon.class);
                    list.add(item);
                }
                fragment.capNhatLV();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public HoaDon getHoaDonByID(final String maHD){
        hoaDon=null;
        ValueEventListener postListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maHD").getValue(String.class).equals(maHD)) {
                        hoaDon=new HoaDon();
                        hoaDon.setMaHD(data.child("maHD").getValue(String.class));
                        hoaDon.setNgayMua(data.child("ngayMua").getValue(Date.class));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(postListener);
        return hoaDon;
    }

    //thêm hóa đơn
    public void insert(HoaDon item) {
        keyID = mDatabase.push().getKey();
        mDatabase.child(keyID).setValue(item)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Thêm thành công", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Thêm thất bại", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //sửa hóa đơn
    public void update(final HoaDon item) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maHD").getValue(String.class).equals(item.getMaHD())) {
                        keyID = data.getKey();
                        mDatabase.child(keyID).setValue(item)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Sửa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Sửa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //xóa hóa đơn
    public void delete(final HoaDon item) {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maHD").getValue(String.class).equals(item.getMaHD())) {
                        keyID = data.getKey();
                        mDatabase.child(keyID).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
