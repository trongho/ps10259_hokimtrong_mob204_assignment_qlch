package com.example.bookhousemanager.dao;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.example.bookhousemanager.fragment.SachFragment;
import com.example.bookhousemanager.fragment.TheLoaiFragment;
import com.example.bookhousemanager.model.Sach;
import com.example.bookhousemanager.model.TheLoai;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class SachDao {
    private DatabaseReference mDatabase;
    private Context context;
    String keyID;
    SachFragment fragment;
    Sach sach;

    public SachDao(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("Sach");
        this.context = context;
    }

    public SachDao(Context context, SachFragment fragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("Sach");
        this.context = context;
        this.fragment=fragment;
    }

    //lấy tất cả sách
    public List<Sach> getAll() {
        final List<Sach> list = new ArrayList<Sach>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Sach item = data.getValue(Sach.class);
                    list.add(item);
                }
                fragment.capNhatLV();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public Sach getSachByID(final String maSach){
        sach=null;
        ValueEventListener postListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maSach").getValue(String.class).equals(maSach)) {
                        sach=new Sach();
                        sach.setMaSach(maSach);
                        sach.setTenSach(data.child("tenSach").getValue(String.class));
                        sach.setMaTL(data.child("matL").getValue(String.class));
                        sach.setTacGia(data.child("tacGia").getValue(String.class));
                        sach.setNxb(data.child("nxb").getValue(String.class));
                        sach.setGiaBia(data.child("giaBia").getValue(Integer.class));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(postListener);
        return sach;
    }


    //thêm sách
    public void insert(Sach item) {
        keyID = mDatabase.push().getKey();
        mDatabase.child(keyID).setValue(item)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Thêm thành công", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Thêm thất bại", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //sửa sách
    public void update(final Sach item) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maSach").getValue(String.class).equals(item.getMaSach())) {
                        keyID = data.getKey();
                        mDatabase.child(keyID).setValue(item)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Sửa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Sửa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //xóa sách
    public void delete(final Sach item) {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maSach").getValue(String.class).equals(item.getMaSach())) {
                        keyID = data.getKey();
                        mDatabase.child(keyID).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
