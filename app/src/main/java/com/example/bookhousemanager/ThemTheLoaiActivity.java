package com.example.bookhousemanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.bookhousemanager.dao.TheLoaiDao;
import com.example.bookhousemanager.model.TheLoai;

public class ThemTheLoaiActivity extends AppCompatActivity {
    EditText edtMaTL,edtTenTL,edtMoTa,edtViTri;
    Button btnThem,btnHuy;
    TheLoaiDao theLoaiDao;
    TheLoai theLoai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_the_loai);
        setTitle("Thêm thể loại");

        initComponent();
        themTheLoai();
        huy();
    }

    public void initComponent(){
        edtMaTL=findViewById(R.id.edtMaTL);
        edtTenTL=findViewById(R.id.edtTenTL);
        edtMoTa=findViewById(R.id.edtMoTa);
        edtViTri=findViewById(R.id.edtViTri);
        btnThem=findViewById(R.id.btnThem);
        btnHuy=findViewById(R.id.btnHuy);
    }

    public void themTheLoai(){
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                theLoai=new TheLoai();
                theLoai.setMaTL(edtMaTL.getText().toString());
                theLoai.setTenTL(edtTenTL.getText().toString());
                theLoai.setMoTa(edtMoTa.getText().toString());
                theLoai.setViTri(Integer.parseInt(edtViTri.getText().toString()));
                theLoaiDao=new TheLoaiDao(ThemTheLoaiActivity.this);
                theLoaiDao.insert(theLoai);
                finish();
            }
        });
    }

    public void huy(){
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
