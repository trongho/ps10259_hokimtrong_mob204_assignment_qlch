package com.example.bookhousemanager;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bookhousemanager.dao.SachDao;
import com.example.bookhousemanager.model.Sach;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SuaSachActivity extends AppCompatActivity {
    Button btnSua,btnHuy;
    EditText edtMaSach,edtTenSach,edtTacGia,edtNXB,edtGiaBia,edtSoLuong;
    Spinner spMaTL;
    TextView tvMaTL;
    Sach sach;
    SachDao sachDao;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_sach);
        setTitle("Sửa sách");

        initComponent();
        suaSach();
        huy();
    }

    public void initComponent(){
        btnSua=findViewById(R.id.btnSua);
        btnHuy=findViewById(R.id.btnHuy);
        edtMaSach=findViewById(R.id.edtMaSach);
        edtTenSach=findViewById(R.id.edtTenSach);
        edtTacGia=findViewById(R.id.edtTacGia);
        edtNXB=findViewById(R.id.edtNXB);
        edtGiaBia=findViewById(R.id.edtGiaBia);
        edtSoLuong=findViewById(R.id.edtSoLuong);
        spMaTL=findViewById(R.id.spMaTL);
        tvMaTL=findViewById(R.id.tvMaTL);
    }

    public void showSpinner(){
        mDatabase = FirebaseDatabase.getInstance().getReference("TheLoai");
        final List<String> list=new ArrayList<>();
        ValueEventListener postListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data:dataSnapshot.getChildren()){
                    String ma=data.child("maTL").getValue(String.class);
                    String ten=data.child("tenTL").getValue(String.class);
                    String item=ma+"-"+ten;
                    list.add(item);

                    ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(
                            SuaSachActivity.this,android.R.layout.simple_spinner_item,list);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spMaTL.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    public void selectItemSpinner(){
        spMaTL.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvMaTL.setText(spMaTL.getSelectedItem().toString().
                        substring(0,spMaTL.getSelectedItem().toString().indexOf("-")));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void layDuLieuTuBundle(){
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("suasach");

        if(bundle!=null) {
            String maTL=bundle.getString("matl");
            String maSach = bundle.getString("masach");
            String tenSach = bundle.getString("tensach");
            String tacGia = bundle.getString("tacgia");
            String nxb = bundle.getString("nxb");
            double giaBia = bundle.getDouble("giabia");
            int soLuong = bundle.getInt("soluong");

            tvMaTL.setText(maTL);
            edtMaSach.setText(maSach);
            edtTenSach.setText(tenSach);
            edtTacGia.setText(tacGia);
            edtNXB.setText(nxb);
            edtGiaBia.setText(giaBia + "");
            edtSoLuong.setText(soLuong + "");
            edtMaSach.setEnabled(false);
        }
    }

    public void suaSach(){
        layDuLieuTuBundle();
        showSpinner();
        selectItemSpinner();

        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sach=new Sach();
                sachDao=new SachDao(SuaSachActivity.this);
                sach.setMaTL(tvMaTL.getText().toString());
                sach.setMaSach(edtMaSach.getText().toString());
                sach.setTenSach(edtTenSach.getText().toString());
                sach.setTacGia(edtTacGia.getText().toString());
                sach.setNxb(edtNXB.getText().toString());
                sach.setGiaBia(Double.parseDouble(edtGiaBia.getText().toString()));
                sach.setSoLuong(Integer.parseInt(edtSoLuong.getText().toString()));
                sachDao.update(sach);
                finish();
            }
        });
    }

    public void huy(){
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
