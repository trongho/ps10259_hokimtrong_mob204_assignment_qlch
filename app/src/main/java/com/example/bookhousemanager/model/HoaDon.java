package com.example.bookhousemanager.model;

import java.util.Date;

public class HoaDon {
    private String maHD;
    private Date ngayMua;

    public HoaDon() {
    }

    public HoaDon(String maHD, Date ngayMua) {
        this.maHD = maHD;
        this.ngayMua = ngayMua;
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public Date getNgayMua() {
        return ngayMua;
    }

    public void setNgayMua(Date ngayMua) {
        this.ngayMua = ngayMua;
    }
}
