package com.example.bookhousemanager.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookhousemanager.DanhSachTaiKhoanActivity;
import com.example.bookhousemanager.R;
import com.example.bookhousemanager.SuaTheLoaiActivity;
import com.example.bookhousemanager.dao.TaiKhoanDao;
import com.example.bookhousemanager.dao.TheLoaiDao;

import com.example.bookhousemanager.fragment.TheLoaiFragment;
import com.example.bookhousemanager.model.TheLoai;

import java.util.List;

public class TheLoaiAdapter extends BaseAdapter {
    List<TheLoai> list;
    public Context context;
    public LayoutInflater inflater;
    TheLoaiDao theLoaiDao;
    TheLoaiAdapter.ViewHolder holder;
    TheLoaiFragment fragment;

    public TheLoaiAdapter(List<TheLoai> list, Context context, TheLoaiFragment fragment) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        theLoaiDao= new TheLoaiDao(context);
        this.fragment=fragment;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    public static class ViewHolder {
        ImageView ivTheLoaiIcon;
        TextView tvMaTL;
        TextView tvTenTL;
        ImageView ivDelete;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final TheLoai theLoai = list.get(position);
        if (convertView == null) {
            holder = new TheLoaiAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_theloai, null);

            //ánh xạ
            holder.ivTheLoaiIcon= (ImageView) convertView.findViewById(R.id.ivTheLoaiIcon);
            holder.tvMaTL= (TextView) convertView.findViewById(R.id.tvMaTL);
            holder.tvTenTL = (TextView) convertView.findViewById(R.id.tvTenTL);
            holder.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);

            //set data lên layout custom


            holder.tvMaTL.setText(theLoai.getMaTL());
            holder.tvTenTL.setText(theLoai.getTenTL());

            convertView.setTag(holder);
        } else
            holder = (TheLoaiAdapter.ViewHolder) convertView.getTag();

        //xóa
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.xoaTheLoai(theLoai);
            }
        });


        //sửa
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SuaTheLoaiActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("maTL",theLoai.getMaTL());
                bundle.putString("tenTL",theLoai.getTenTL());
                bundle.putString("moTa",theLoai.getMoTa());
                bundle.putInt("viTri",theLoai.getViTri());

                intent.putExtra("suaTheLoai",bundle);
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
