package com.example.bookhousemanager.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookhousemanager.DanhSachTaiKhoanActivity;
import com.example.bookhousemanager.R;
import com.example.bookhousemanager.dao.TaiKhoanDao;
import com.example.bookhousemanager.model.TaiKhoan;

import java.util.List;

public class TaiKhoanAdapter extends BaseAdapter {
    List<TaiKhoan> list;
    public Activity context;
    public LayoutInflater inflater;
    TaiKhoanDao taiKhoanDao;
    ViewHolder holder;

    public TaiKhoanAdapter(List<TaiKhoan> list, Activity context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        taiKhoanDao = new TaiKhoanDao(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    public static class ViewHolder {
        ImageView ivTaiKhoanIcon;
        TextView tvName;
        TextView tvPhone;
        ImageView ivDelete;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final TaiKhoan taiKhoan = list.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_taikhoan, null);

            //ánh xạ
            holder.ivTaiKhoanIcon = (ImageView) convertView.findViewById(R.id.ivTaiKhoanIcon);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvPhone = (TextView) convertView.findViewById(R.id.tvPhone);
            holder.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        //set data lên layout custom
        if (position % 3 == 0) {
            holder.ivTaiKhoanIcon.setImageResource(R.drawable.emone);
        } else if (position % 3 == 1) {
            holder.ivTaiKhoanIcon.setImageResource(R.drawable.emtwo);
        } else {
            holder.ivTaiKhoanIcon.setImageResource(R.drawable.emthree);
        }
        holder.tvName.setText(taiKhoan.getFullName());
        holder.tvPhone.setText(taiKhoan.getPhone());

        //xóa tài khoản
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DanhSachTaiKhoanActivity) context).xoaTaiKhoan(taiKhoan);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Ban vua click dong "+position, Toast.LENGTH_SHORT).show();
            }
        });


        return convertView;
    }

    public void changeDataset(List<TaiKhoan> items){
        this.list = items;
        notifyDataSetChanged();
    }
}
