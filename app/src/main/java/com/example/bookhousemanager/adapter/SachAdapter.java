package com.example.bookhousemanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Contacts;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookhousemanager.R;
import com.example.bookhousemanager.SuaSachActivity;
import com.example.bookhousemanager.dao.TheLoaiDao;
import com.example.bookhousemanager.fragment.SachFragment;
import com.example.bookhousemanager.model.Sach;
import com.example.bookhousemanager.model.TheLoai;

import java.util.List;

public class SachAdapter extends RecyclerView.Adapter<SachAdapter.ViewHolder> {
    static Context context;
    List<Sach> list;
    TheLoaiDao theLoaiDao;
    SachFragment fragment;
    Sach sach;

    public SachAdapter(Context context,List<Sach> list, SachFragment fragment) {
        this.context=context;
        this.list = list;
        this.fragment = fragment;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public RelativeLayout rl_layout;
        public TextView tvMaSach,tvTenSach,tvSoLuong;
        public ImageView ivSachIcon,ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rl_layout=itemView.findViewById(R.id.rl_layout);
            tvMaSach=itemView.findViewById(R.id.tvMaSach);
            tvTenSach=itemView.findViewById(R.id.tvTenSach);
            tvSoLuong=itemView.findViewById(R.id.tvSoLuong);
            ivSachIcon=itemView.findViewById(R.id.ivSachIcon);
            ivDelete=itemView.findViewById(R.id.ivDelete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sach=list.get(getAdapterPosition());
                    Intent intent=new Intent(context, SuaSachActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("matl",sach.getMaTL());
                    bundle.putString("masach",sach.getMaSach());
                    bundle.putString("tensach",sach.getTenSach());
                    bundle.putString("tacgia",sach.getTacGia());
                    bundle.putString("nxb",sach.getNxb());
                    bundle.putDouble("giabia",sach.getGiaBia());
                    bundle.putInt("soluong",sach.getSoLuong());
                    intent.putExtra("suasach",bundle);
                    context.startActivity(intent);
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sach=list.get(getAdapterPosition());
                    fragment.xoaSach(sach);
                }
            });

        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_sach_layout, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        // - lay  phan tu tu danh sach du lieu tai vi tri position
        // - thay the noi dung cua view voi phan tu do
        sach=list.get(i);

        viewHolder.tvMaSach.setText(sach.getMaSach());
        viewHolder.tvTenSach.setText(sach.getTenSach());
        viewHolder.tvSoLuong.setText(sach.getSoLuong()+"");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
