package com.example.bookhousemanager.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookhousemanager.HoaDonChiTietActivity;
import com.example.bookhousemanager.R;
import com.example.bookhousemanager.SuaHoaDonActivity;
import com.example.bookhousemanager.fragment.HoaDonFragment;
import com.example.bookhousemanager.model.HoaDon;
import com.example.bookhousemanager.model.HoaDonChiTiet;

import java.text.SimpleDateFormat;
import java.util.List;

public class HoaDonAdapter extends RecyclerView.Adapter<HoaDonAdapter.ViewHolder> {
    static Context context;
    List<HoaDon> list;
    HoaDonFragment fragment;
    HoaDon hoaDon;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public HoaDonAdapter(Context context,List<HoaDon> list, HoaDonFragment fragment) {
        this.context=context;
        this.list = list;
        this.fragment = fragment;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvMaHD,tvNgayMua;
        public ImageView ivHoaDonIcon,ivEdit,ivDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMaHD=itemView.findViewById(R.id.tvMaHD);
            tvNgayMua=itemView.findViewById(R.id.tvNgayMua);
            ivHoaDonIcon=itemView.findViewById(R.id.ivHoaDonIcon);
            ivEdit=itemView.findViewById(R.id.ivEdit);
            ivDelete=itemView.findViewById(R.id.ivDelete);

            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hoaDon=list.get(getAdapterPosition());
                    Intent intent=new Intent(context, SuaHoaDonActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("mahd",hoaDon.getMaHD());
                    bundle.putString("ngaymua", String.valueOf(sdf.format(hoaDon.getNgayMua())));
                    intent.putExtra("suahoadon",bundle);
                    context.startActivity(intent);
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hoaDon=list.get(getAdapterPosition());
                    fragment.xoaHoaDon(hoaDon);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hoaDon=list.get(getAdapterPosition());
                    Intent intent=new Intent(context, HoaDonChiTietActivity.class);
                    //truyền mã hóa đơn
                    Bundle bundle=new Bundle();
                    bundle.putString("mahd",hoaDon.getMaHD());
                    intent.putExtra("hoadonchitiet",bundle);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_hoa_don_layout, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new HoaDonAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        hoaDon=list.get(i);
        viewHolder.tvMaHD.setText(hoaDon.getMaHD());
        viewHolder.tvNgayMua.setText(sdf.format(hoaDon.getNgayMua()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
