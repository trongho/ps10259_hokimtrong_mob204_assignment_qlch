package com.example.bookhousemanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bookhousemanager.HoaDonChiTietActivity;
import com.example.bookhousemanager.R;
import com.example.bookhousemanager.SuaHoaDonChiTietActivity;
import com.example.bookhousemanager.SuaSachActivity;
import com.example.bookhousemanager.dao.HoaDonChiTietDao;
import com.example.bookhousemanager.dao.TheLoaiDao;
import com.example.bookhousemanager.fragment.SachFragment;
import com.example.bookhousemanager.model.HoaDon;
import com.example.bookhousemanager.model.HoaDonChiTiet;
import com.example.bookhousemanager.model.Sach;

import java.util.List;

public class HoaDonChiTietAdapter extends RecyclerView.Adapter<HoaDonChiTietAdapter.ViewHolder> {
    static Context context;
    List<HoaDonChiTiet> list;
    Sach sach;
    HoaDon hoaDon;
    HoaDonChiTietDao hoaDonChiTietDao;
    HoaDonChiTiet hoaDonChiTiet;

    public HoaDonChiTietAdapter(Context context,List<HoaDonChiTiet> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvMaSach,tvSoLuongMua,tvGiaBia,tvThanhTien;
        public ImageView ivDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMaSach=itemView.findViewById(R.id.tvMaSach);
            tvSoLuongMua=itemView.findViewById(R.id.tvSoLuongMua);
            tvGiaBia=itemView.findViewById(R.id.tvGiaBia);
            tvThanhTien=itemView.findViewById(R.id.tvThanhTien);
            ivDelete=itemView.findViewById(R.id.ivDelete);

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hoaDonChiTiet=list.get(getAdapterPosition());
                    ((HoaDonChiTietActivity)context).xoaHDCT(hoaDonChiTiet);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sach=new Sach();
                    hoaDon=new HoaDon();
                    hoaDonChiTiet=list.get(getAdapterPosition());
                    Intent intent=new Intent(context, SuaHoaDonChiTietActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("mahd",hoaDonChiTiet.getHoaDon()+"");
                    bundle.putString("mahdct",hoaDonChiTiet.getMaHDCT()+"");
                    bundle.putString("masach",hoaDonChiTiet.getSach()+"");
                    bundle.putString("soluongmua",hoaDonChiTiet.getSoLuongMua()+"");
                    intent.putExtra("suahdct",bundle);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_hoa_don_chi_tiet_layout, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new HoaDonChiTietAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        hoaDonChiTiet=list.get(i);

        viewHolder.tvMaSach.setText(hoaDonChiTiet.getSach().getMaSach());
        viewHolder.tvSoLuongMua.setText(hoaDonChiTiet.getSoLuongMua()+"");
        viewHolder.tvGiaBia.setText(hoaDonChiTiet.getSach().getGiaBia()+"");
        viewHolder.tvThanhTien.setText(hoaDonChiTiet.getSach().getGiaBia()*hoaDonChiTiet.getSoLuongMua()+"");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
