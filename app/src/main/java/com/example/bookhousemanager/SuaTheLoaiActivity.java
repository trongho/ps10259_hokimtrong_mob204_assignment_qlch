package com.example.bookhousemanager;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bookhousemanager.adapter.TheLoaiAdapter;
import com.example.bookhousemanager.dao.TheLoaiDao;
import com.example.bookhousemanager.fragment.TheLoaiFragment;
import com.example.bookhousemanager.model.TheLoai;

public class SuaTheLoaiActivity extends AppCompatActivity {
    EditText edtMaTL, edtTenTL, edtMoTa, edtViTri;
    Button btnSua, btnHuy;
    TheLoaiDao theLoaiDao;
    TheLoai theLoai;
    TheLoaiAdapter theLoaiAdapter;
    TheLoaiFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_the_loai);
        setTitle("Sửa thể loại");

        initComponent();
        suaTheLoai();
        huy();
    }


    public void initComponent() {
        edtMaTL = findViewById(R.id.edtMaTL);
        edtTenTL = findViewById(R.id.edtTenTL);
        edtMoTa = findViewById(R.id.edtMoTa);
        edtViTri = findViewById(R.id.edtViTri);
        btnSua = findViewById(R.id.btnSua);
        btnHuy = findViewById(R.id.btnHuy);
    }

    public void layDuLieu() {
        // lay data tu bundle
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("suaTheLoai");
        if (bundle != null) {
            String maTL = bundle.getString("maTL", "");
            String tenTL = bundle.getString("tenTL", "");
            String moTa = bundle.getString("moTa", "");
            int vitri = bundle.getInt("viTri", 0);
            //dua data len EditText
            edtMaTL.setText(maTL);
            edtTenTL.setText(tenTL);
            edtMoTa.setText(moTa);
            edtViTri.setText(vitri+"");
            // khoa EditText MaSach
            if (!maTL.isEmpty()) {
                edtMaTL.setEnabled(false);
            }
        }
    }

    public void suaTheLoai() {
        layDuLieu();
        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                theLoai = new TheLoai();
                theLoai.setMaTL(edtMaTL.getText().toString());
                theLoai.setTenTL(edtTenTL.getText().toString());
                theLoai.setMoTa(edtMoTa.getText().toString());
                theLoai.setViTri(Integer.parseInt(edtViTri.getText().toString()));
                theLoaiDao = new TheLoaiDao(SuaTheLoaiActivity.this);
                theLoaiDao.update(theLoai);
                finish();
            }
        });
    }

    public void huy() {
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
