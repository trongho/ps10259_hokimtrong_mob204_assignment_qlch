package com.example.bookhousemanager.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.bookhousemanager.R;


public class HomeFragment extends Fragment {
    BottomNavigationView bottomNavigationView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //load thống kê fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().add(R.id.frame_thongke_layout, new ThongKeFragment()).commit();
        //xử lý bottom navigation
        setBottomNavigationItemSelected();

    }

    public void setBottomNavigationItemSelected(){
        bottomNavigationView =getView().findViewById(R.id.bottom_navigation_home);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentManager fragmentManager = getFragmentManager();
                switch (menuItem.getItemId()) {
                    case R.id.navigation_thongke:
                        fragmentManager.beginTransaction().replace(R.id.frame_thongke_layout, new ThongKeFragment()).commit();
                        return true;
                    case R.id.navigation_sachbanchay:
                        fragmentManager.beginTransaction().replace(R.id.frame_thongke_layout, new SachBanChayFragment()).commit();
                        return true;
                }
                return false;
            }
        });
    }
}
