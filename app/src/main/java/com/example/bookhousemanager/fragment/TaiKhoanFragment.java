package com.example.bookhousemanager.fragment;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.bookhousemanager.DanhSachTaiKhoanActivity;
import com.example.bookhousemanager.R;
import com.example.bookhousemanager.SuaTaiKhoanActivity;
import com.example.bookhousemanager.dao.TaiKhoanDao;
import com.example.bookhousemanager.model.TaiKhoan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.getIntent;
import static android.content.Intent.getIntentOld;
import static android.support.constraint.Constraints.TAG;


public class TaiKhoanFragment extends Fragment {
    TextView tvUsername, tvPassword, tvFullName, tvPhone;
    Button btnDanhSachTaiKhoan, btnSuaTaiKhoan;
    TaiKhoanDao taiKhoanDao;
    private DatabaseReference mDatabase;

    public TaiKhoanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tai_khoan, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Tài khoản");

        initComponent();
        getCurentUser();
        danhSachTaiKhoan();
        suaTaiKhoan();

    }

    public void initComponent() {
        tvUsername = getView().findViewById(R.id.tvUsername);
        tvPassword = getView().findViewById(R.id.tvPassword);
        tvFullName = getView().findViewById(R.id.tvFullName);
        tvPhone = getView().findViewById(R.id.tvPhone);
        btnDanhSachTaiKhoan = getView().findViewById(R.id.btnDanhSachTaiKhoan);
        btnSuaTaiKhoan = getView().findViewById(R.id.btnSuaTaiKhoan);
    }

    public void getCurentUser() {
        mDatabase = FirebaseDatabase.getInstance().getReference("TaiKhoan");

        //lấy username từ login
        Intent intent = getActivity().getIntent();
        final String username = intent.getStringExtra("username");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("username").getValue(String.class).equalsIgnoreCase(username)) {
                        TaiKhoan item = data.getValue(TaiKhoan.class);
                        String username = item.getUsername();
                        String password = item.getPassword();
                        String fullname = item.getFullName();
                        String phone = item.getPhone();
                        tvUsername.setText(username);
                        tvPassword.setText(password);
                        tvFullName.setText(fullname);
                        tvPhone.setText(phone);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);

    }


    public void danhSachTaiKhoan() {
        btnDanhSachTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DanhSachTaiKhoanActivity.class);
                startActivity(intent);
            }
        });
    }

    public void suaTaiKhoan() {
        btnSuaTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SuaTaiKhoanActivity.class);
                intent.putExtra("username", tvUsername.getText());
                intent.putExtra("password", tvPassword.getText());
                intent.putExtra("fullname", tvFullName.getText());
                intent.putExtra("phone", tvPhone.getText());
                startActivity(intent);
            }
        });
    }

}
