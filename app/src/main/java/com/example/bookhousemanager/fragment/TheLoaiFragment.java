package com.example.bookhousemanager.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.bookhousemanager.DanhSachTaiKhoanActivity;
import com.example.bookhousemanager.R;
import com.example.bookhousemanager.ThemTheLoaiActivity;
import com.example.bookhousemanager.adapter.TaiKhoanAdapter;
import com.example.bookhousemanager.adapter.TheLoaiAdapter;
import com.example.bookhousemanager.dao.TaiKhoanDao;
import com.example.bookhousemanager.dao.TheLoaiDao;
import com.example.bookhousemanager.model.TaiKhoan;
import com.example.bookhousemanager.model.TheLoai;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TheLoaiFragment extends Fragment {
    ListView lvDanhSachTheLoai;
    TheLoaiAdapter adapter;
    List<TheLoai> list;
    TheLoaiDao theLoaiDao;


    public TheLoaiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_the_loai, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //toolbar
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Thể loại");

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.show();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                themTheLoai();
            }
        });

        initComponent();
        showDanhSachTheLoai();
    }

    public void initComponent(){
        lvDanhSachTheLoai=getView().findViewById(R.id.lvDanhSachTheLoai);
    }

    public void showDanhSachTheLoai(){
        list=new ArrayList<>();
        theLoaiDao=new TheLoaiDao(getActivity(),this);
        list= theLoaiDao.getAll();
        adapter=new TheLoaiAdapter(list,getContext(),this);
        lvDanhSachTheLoai.setAdapter(adapter);
    }

    public void themTheLoai(){
        Intent intent=new Intent(getActivity(), ThemTheLoaiActivity.class);
        startActivity(intent);
    }

    public void xoaTheLoai(TheLoai theLoai){
        theLoaiDao.delete(theLoai);
        capNhatLV();
    }

    public void capNhatLV(){
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        capNhatLV();
    }
}
