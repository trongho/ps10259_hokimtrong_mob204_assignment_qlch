package com.example.bookhousemanager.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.bookhousemanager.R;
import com.example.bookhousemanager.ThemHoaDonActivity;
import com.example.bookhousemanager.ThemSachActivity;
import com.example.bookhousemanager.adapter.HoaDonAdapter;
import com.example.bookhousemanager.dao.HoaDonDao;
import com.example.bookhousemanager.model.HoaDon;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HoaDonFragment extends Fragment {
    RecyclerView recyclerView;
    HoaDon hoaDon;
    List<HoaDon> list;
    HoaDonAdapter hoaDonAdapter;
    HoaDonDao hoaDonDao;
    LinearLayoutManager linearLayoutManager;

    public HoaDonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.sach_option_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_them:
                Intent intent=new Intent(getActivity(), ThemHoaDonActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hoa_don, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar=getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Hóa đơn");
        recyclerView=view.findViewById(R.id.recycle_view);

        showDanhSachHoaDon();
    }



    public void showDanhSachHoaDon(){
        hoaDonDao=new HoaDonDao(getContext(),this);
        list=new ArrayList<HoaDon>();
        list=hoaDonDao.getAll();
        linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        hoaDonAdapter=new HoaDonAdapter(getActivity(),list,this);
        recyclerView.setAdapter(hoaDonAdapter);


    }

    public void xoaHoaDon(HoaDon hoaDon){
        hoaDonDao.delete(hoaDon);
        capNhatLV();
    }

    public void capNhatLV(){
        hoaDonAdapter.notifyItemInserted(list.size());
        hoaDonAdapter.notifyDataSetChanged();
    }
}
