package com.example.bookhousemanager.fragment;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.bookhousemanager.R;
import com.example.bookhousemanager.SuaSachActivity;
import com.example.bookhousemanager.ThemSachActivity;
import com.example.bookhousemanager.adapter.SachAdapter;
import com.example.bookhousemanager.dao.SachDao;
import com.example.bookhousemanager.model.Sach;
import com.example.bookhousemanager.model.TheLoai;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SachFragment extends Fragment{

    RecyclerView recyclerView;
    List<Sach> list;
    Sach sach;
    LinearLayoutManager mLayoutManager;
    SachDao sachDao;
    SachAdapter sachAdapter;

    public SachFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.sach_option_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_them:
                Intent intent=new Intent(getActivity(), ThemSachActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sach, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //toolbar
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Sách");

        recyclerView=view.findViewById(R.id.recycle_view);
        showSach();
    }

    public void showSach(){
        sachDao=new SachDao(getContext(),this);
        list=new ArrayList<Sach>();
        list=sachDao.getAll();
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        sachAdapter=new SachAdapter(getContext(),list,this);
        recyclerView.setAdapter(sachAdapter);
    }

    public void xoaSach(Sach sach){
        sachDao.delete(sach);
        capNhatLV();
    }

    public void capNhatLV(){
        sachAdapter.notifyItemInserted(list.size());
        sachAdapter.notifyDataSetChanged();
    }
}
