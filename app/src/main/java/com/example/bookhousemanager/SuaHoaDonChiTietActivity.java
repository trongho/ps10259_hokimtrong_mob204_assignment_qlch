package com.example.bookhousemanager;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bookhousemanager.dao.HoaDonChiTietDao;
import com.example.bookhousemanager.model.HoaDon;
import com.example.bookhousemanager.model.HoaDonChiTiet;
import com.example.bookhousemanager.model.Sach;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class SuaHoaDonChiTietActivity extends AppCompatActivity {
    EditText edtMaHD,edtMaHDCT,edtSoLuongMua;
    TextView tvMaSach;
    Spinner spMaSach;
    Button btnSua,btnHuy;
    HoaDonChiTiet hoaDonChiTiet;
    HoaDonChiTietDao hoaDonChiTietDao;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseSach;
    Sach sach;
    HoaDon hoaDon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sua_hoa_don_chi_tiet);

        initComponent();
        sua();
        huy();
    }

    public void initComponent(){
        edtMaHD=findViewById(R.id.edtMaHD);
        edtMaHDCT=findViewById(R.id.edtMaHDCT);
        tvMaSach=findViewById(R.id.tvMaSach);
        spMaSach=findViewById(R.id.spMaSach);
        edtSoLuongMua=findViewById(R.id.edtSoLuongMua);
        btnSua=findViewById(R.id.btnSua);
        btnHuy=findViewById(R.id.btnHuy);
    }

    public void showSpinner(){
        mDatabase = FirebaseDatabase.getInstance().getReference("Sach");
        final List<String> list=new ArrayList<>();
        ValueEventListener postListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data:dataSnapshot.getChildren()){
                    String ma=data.child("maSach").getValue(String.class);
                    String ten=data.child("tenSach").getValue(String.class);
                    String item=ma+"-"+ten;
                    list.add(item);

                    ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(
                            SuaHoaDonChiTietActivity.this,android.R.layout.simple_spinner_item,list);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spMaSach.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    public void selectItemSpinner(){
        spMaSach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvMaSach.setText(spMaSach.getSelectedItem().toString().
                        substring(0,spMaSach.getSelectedItem().toString().indexOf("-")));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void layDuLieuTuBundle(){
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("suahdct");

        if(bundle!=null) {
            String maHD=bundle.getString("mahd");
            String maHDCT = bundle.getString("mahdct");
            String maSach = bundle.getString("masach");
            String soLuongMua = bundle.getString("soluongmua");

            tvMaSach.setText(maSach);
            edtMaHD.setText(maHD);
            edtMaHDCT.setText(maHDCT);
            edtSoLuongMua.setText(soLuongMua+"");
            edtMaHD.setEnabled(false);
            edtMaHDCT.setEnabled(false);
        }
    }

    public void sua(){
        showSpinner();
        selectItemSpinner();
        layDuLieuTuBundle();

        btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sach=new Sach();
                hoaDon=new HoaDon();
                hoaDonChiTiet=new HoaDonChiTiet();
                hoaDonChiTietDao=new HoaDonChiTietDao(SuaHoaDonChiTietActivity.this);

                mDatabaseSach = FirebaseDatabase.getInstance().getReference("Sach");
                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            if (data.child("maSach").getValue(String.class).equals(tvMaSach.getText().toString())) {
                                sach = data.getValue(Sach.class);
                                hoaDonChiTiet.setMaHDCT(Integer.parseInt(edtMaHDCT.getText().toString()));
                                hoaDonChiTiet.setSach(sach);
                                hoaDonChiTiet.setHoaDon(hoaDon);
                                hoaDonChiTiet.setSoLuongMua(Integer.parseInt(edtSoLuongMua.getText().toString()));
                                hoaDonChiTietDao.update(hoaDonChiTiet);
                                finish();
                            }
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                };
                mDatabaseSach.addValueEventListener(postListener);
            }
        });

    }
    public void huy(){
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
